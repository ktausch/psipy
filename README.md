psi.py is a standalone Python file which defines functions that calculate two different goodness-of-fit statistics: Pearson's chi-squared statistic (normalized by the number of degrees of freedom) and the psi-squared statistic introduced in Tauscher et al. (2018). In both cases, to use psi.py, put a copy of it in the directory from which you wish to call Python and add the following import to the top of the script from which you wish to call it:

`from psi import chi_squared, psi_squared`

Documentation for the two functions is included in psi.py and can also be accessed through IPython's help features (just call `chi_squared??` or `psi_squared??` in IPython to see them). There is also an example included in the repository which can be run directly with Python. It shows how to use the chi\_squared and psi\_squared functions (in their simple forms).

The only dependency of the psi.py script is numpy (which performs the correlation and is used for it's multi-dimensional array support). The example also has matplotlib as a dependency so that it can make histograms of chi\_squared and psi\_squared statistics and show it to the user.

If you have any questions, contact me at Keith.Tauscher@colorado.edu for support!

If you use psipy as part of your work, please cite the following paper: https://arxiv.org/abs/1810.00076 (to be updated when published).
